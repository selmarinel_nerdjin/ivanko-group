<?php

namespace App\Http\Controllers;

use App\Http\Modules\Files\Models\File;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Modules\Projects\Models\Project as Model;
use App\Http\Modules\Order\Models\Order;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function orders(){
        $collection = Order::query()->paginate(10);
        return view('orders',['collection'=>$collection]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection = Model::query()->where('status','!=',Model::$DELETE)->orderby('created_at','desc')->paginate(3);
        return view('home',['collection'=>$collection]);
    }

    public function active(Request $request,$id){
        $model = Model::query()->find($id);
        if (!$model){
            return redirect('/adminpanel?error');
        }
        $model->status = ($model->status)?Model::$INACTIVE:Model::$ACTIVE;
        $model->save();
        return redirect('/adminpanel');
    }

    public function delete(Request $request,$id){
        $model = Model::query()->find($id);
        if (!$model){
            return redirect('/adminpanel?error');
        }
        $model->status = Model::$DELETE;
        $model->save();
        return redirect('/adminpanel');
    }

    public function add(Request $request){
        if($request->method() == "GET"){
            $model = new Model();
            return view('addProjext',['model'=>$model]);
        }

        $data = [
            'title' =>  $request->input('title'),
            'description'  =>  $request->input('description'),
            'file_cover'  =>  $request->file('file_cover'),
        ];
        $model = new Model();
        $image = $request->file('file_cover');
        if (isset($image)) {
            $cover = $this->saveFile();
            $data['cover'] = $cover;
        }

        $model->fill($data);
        $model->save();
        return redirect('/adminpanel');
    }

    public function edit(Request $request,$id){
        if($request->method() == "GET"){
            $model = Model::query()->find($id);
            if (!$model){
                return redirect('/adminpost');
            }
            return view('addProjext',['model'=>$model]);
        }

        $data = [
            'id'    =>  $request->input('id'),
            'title' =>  $request->input('title'),
            'description'  =>  $request->input('description'),
            'file_cover'  =>  $request->file('file_cover'),
        ];

        $model = Model::query()->find($id);
        $image = $request->file('file_cover');
        if (isset($image)) {
            $cover = $this->saveFile();
            $data['cover'] = $cover;
        }
        $model->fill($data);
        $model->save();
        return redirect('/adminpanel');
    }

    public function orderAccept(Request $request,$id){
        $model = Order::query()->find($id);
        if (!$model){
            return redirect('/orders?error=not_found_order');
        }
        if ($model->status == Order::$InVoice){
            $model->status = Order::$InProcess;
        }
        elseif($model->status == Order::$InProcess){
            $model->status = Order::$EMailFail;
            if ($model->mail) {
                if (filter_var($model->mail, FILTER_VALIDATE_EMAIL)){
                    Mail::send('Complete', ['model'=>$model], function ($message) use ($model) {
                        $message->to($model->mail, 'IvankoGroup')->subject('Ваш заказ выполнен');
                    });
                    $model->status = Order::$Complete;
                }
            }
        }
        $model->save();
        return redirect('/orders');
    }
    public function orderDecline(Request $request,$id){
        $model = Order::query()->find($id);
        if (!$model){
            return redirect('/orders?error=not_found_order');
        }
        $model->status = Order::$Decline;
        $model->save();
        return redirect('/orders');
    }

    public function addFile(Request $request,$id){
        $data = $request->all();
        $image = $request->file('new_photo');
        if (isset($image)) {
            $cover = $this->saveFile('new_photo');
            $data['cover'] = $cover;
        }
        $data['project_id'] = $id;
        $model = new File();
        $model->fill($data);
        $model->save();
        return redirect('/proj/'.$id.'/edit');
    }

    public function fileEdit(Request $request,$id){
        $model = File::query()->find($id);
        if (!$model){
            return redirect('/adminpanel');
        }
        if ($request->method() == 'GET'){
            return view('fileEdit',['model'=>$model]);
        }
        $data = $request->all();
        $image = $request->file('file_cover');
        if (isset($image)) {
            $cover = $this->saveFile('file_cover');
            $data['cover'] = $cover;
        }
        $model->fill($data);
        $model->save();
        return redirect('/files-list');
    }
    public function fileDelete(Request $request,$id){
        $model = File::query()->find($id);
        if (!$model){
            return redirect('/adminpanel');
        }
        $model->status = ($model->status)?Model::$INACTIVE:Model::$ACTIVE;;
        $model->save();
        return redirect('/files-list');
    }

    public function filesIndex(){
        $collection = File::query()->where('status','!=',Model::$DELETE)->orderby('created_at','desc')->paginate(5);
        return view('filesList',['collection'=>$collection]);
    }
}
