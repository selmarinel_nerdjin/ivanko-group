<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 11.02.2016
 * Time: 16:33
 */

namespace App\Http\Controllers;

use App\Http\Modules\Comments\Models\Comment as Model;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showComments(Request $request,$id){
        $model = Model::query()->where('project_id',$id)->orderBy('id','desc')->paginate(20);
        if(!$model){
            return redirect('/?error=no_found_project');
        }
        return $model;

    }

    public function index(Request $request){
        $collection = Model::query()->where('status','!=',3)->orderby('created_at','desc')->paginate('5');
        return view('comments',['collection'=>$collection]);
    }

    public function delete(Request $request,$id){
        $model = Model::query()->find($id);
        if (!$model){
            return redirect('/comments?error');
        }
        $model->status = 3;
        $model->save();
        return redirect('/comments');
    }
    public function active(Request $request,$id){
        $model = Model::query()->find($id);
        if (!$model){
            return redirect('/comments?error');
        }
        $model->status = ($model->status)?0:1;
        $model->save();
        return redirect('/comments');
    }
}