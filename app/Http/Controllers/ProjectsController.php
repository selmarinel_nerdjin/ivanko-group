<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 07.02.2016
 * Time: 1:26
 */

namespace App\Http\Controllers;


use App\Http\Modules\Projects\Models\Project;
use App\Http\Modules\Comments\Models\Comment;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index(Request $request){
        $collection = Project::query()->where('status',Project::$ACTIVE)->orderBy('id','desc')->paginate(12);
//        $comments = Comment::query()->where('project_id',)
        return view('site.main.page',['collection'=>$collection]);
    }

    public function addView(Request $request,$id){
        $model = Project::query()->find($id);
        if (!$model){
            return 'error';
        }
        $model->views = $model->views +1;
        $model->save();
        $comments = Comment::query()->where('project_id',$id)->orderBy('created_at','desc')->get();
        RETURN 'dffd';
        return $comments;
    }

    public function all(Request $request){
        $collection = Project::query()->where('status',Project::$ACTIVE)->orderBy('id','desc')->paginate(1);
        if (($collection->first())) {
            $this->addView($request, $collection->first()->id);
        }
        return view('site.main.projects',['collection'=>$collection]);
    }

    public function addComments(Request $request,$id){
        $data['project_id'] = $id;
        $data['user'] = $request->input('user');
        $data['text'] = $request->input('text');
        $data['avatar'] = $request->input('avatar');
        $model = new Comment();
        $model->fill($data);
        $model->save();
        return $model;
    }
}