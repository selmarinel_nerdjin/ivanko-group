<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;
use App\Http\Modules\Order\Models\Order as Model;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function go(Request $request)
    {
        Mail::send('Order', $request->all(), function($message)
        {
            $message->to('ivan.slipcov@gmail.com', 'ЗАКАЗ')->subject('Заказ');
            $message->to('ivan.slipcov@ivankogroup.com', 'ЗАКАЗ')->subject('Заказ');
        });
        $model = new Model();
        $model->fill($request->all());
        $model->save();
    return redirect('/#contact');
    }

    public function saveFile($name = 'file_cover',$uploaddir = './files/')
    {
        $file_name = md5(basename($_FILES[$name]['name']) . time()) . strrchr($_FILES[$name]['name'],'.');
        $uploadfile = $uploaddir .$file_name;
        if (copy($_FILES[$name]['tmp_name'], $uploadfile)) {
            echo "<h3>Файл успешно загружен на сервер</h3>";
        } else {
            echo "<h3>Ошибка! Не удалось загрузить файл на сервер!</h3>";
            exit;
        }
        return $file_name;
    }


    protected $client_id = '5168417'; // ID приложения
    protected $client_secret = 'HjMY84Wic3cFn0e4JCDo'; // Защищённый ключ
    protected $redirect_uri = 'http://ivan.loc/vk_auth'; // Адрес сайта
    protected $url = 'http://oauth.vk.com/authorize';
    protected  $user;

    public function redirectToProvider()
    {
        $params = array(
            'client_id'     => $this->client_id,
            'redirect_uri'  => $this->redirect_uri,
            'response_type' => 'code'
        );
        return redirect($this->url . '?' . urldecode(http_build_query($params)));
    }

    private function fileGetContents($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    public function handleProviderCallback(Request $request)
    {
        if (isset($_GET['code'])) {
            $params = array(
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'code' => $_GET['code'],
                'redirect_uri' => $this->redirect_uri
            );
            $token = json_decode($this->fileGetContents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);
            if (isset($token['access_token'])) {
                $params = array(
                    'uids' => $token['user_id'],
                    'fields' => 'uid,first_name,last_name,screen_name,photo_100',
                    'access_token' => $token['access_token']
                );
                $this->user = json_decode($this->fileGetContents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);

                if (isset( $this->user['response'][0]['uid'])) {
                    $this->user =  $this->user['response'][0];
                    setcookie('name',$this->user['first_name']);
                    setcookie('img',$this->user['photo_100']);
                    setcookie('link',$this->user['screen_name']);
                    return redirect('/');
                }
            }
        }
        setcookie('name',null);
        setcookie('img',null);
        setcookie('link',null);
        return redirect('/');
    }
}
