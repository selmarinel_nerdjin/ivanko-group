<?php

namespace App\Http\Modules\Order\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Order extends Model
{
    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    static $InVoice = 1;
    static $InProcess = 2;
    static $Complete = 3;
    static $Decline = 4;
    static $EMailFail = 5;

    private $state = [
        1 => 'Пришёл заказ',
        2 => 'В процессе',
        3 => 'Выполнено',
        4 => 'Отменено',
        5 => 'Выполнено. Не правильная почта.'
    ];

    private $stateStyle = [
        1   =>  'alert-warning',
        2   =>  'alert-info',
        3   =>  'alert-success',
        4   =>  'alert-danger',
        5   =>  'alert-warning'
    ];

    public function getStateName(){
        return $this->state[$this->status];
    }

    public function getStateStyle(){
        return $this->stateStyle[$this->status];
    }

    public function count(){
        return $this->query()->get()->count();
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    public $timestamps = true;

    protected $fillable = array('id', 'name', 'phone', 'mail', 'text','status');

    public function fill(array $attributes) {
        parent::fill($attributes);
    }

}