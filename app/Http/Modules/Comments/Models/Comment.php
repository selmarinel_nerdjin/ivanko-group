<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 11.02.2016
 * Time: 16:30
 */

namespace App\Http\Modules\Comments\Models;


use App\Http\Modules\Projects\Models\Project;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $perPage = 15;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';

    public $timestamps = true;

    protected $fillable = array('id', 'project_id', 'user', 'text','status','avatar');

    public function fill(array $attributes) {
        parent::fill($attributes);
    }

    public function count(){
        return $this->query()->get()->count();
    }

    public function project(){
        return Project::query()->find($this->project_id);
    }

    public function getCover(){
        return ($this->avatar)?$this->avatar:'/images/ava.png';
    }

}