<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 11.02.2016
 * Time: 16:30
 */

namespace App\Http\Modules\Files\Models;


use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $baselink = 'Http://ivankogroup.com';

    protected $perPage = 15;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'files';

    public $timestamps = true;

    protected $fillable = array('id', 'project_id', 'name','info','cover','text','status');

    public function fill(array $attributes) {
        parent::fill($attributes);
    }

    public function count(){
        return $this->query()->get()->count();
    }

    public function getCoverAttribute($cover){
        if (strripos($cover,':/')){
            return $cover;
        }
        if (is_file($cover)){
            return $cover;
        }
        $r = './files/';
//        $r='';
        if (is_file($r . $cover)) {
            return $this->baselink . $r . $cover;
        }

        return '/images/none.jpg';
    }

    public function project(){
        return $this->hasOne('App\Http\Modules\Projects\Models\Project','id','project_id');
    }

}