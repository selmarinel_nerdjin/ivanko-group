<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 07.02.2016
 * Time: 1:29
 */

namespace App\Http\Modules\Projects\Models;


use Dotenv\Dotenv;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Project extends Model
{
    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    static $ACTIVE = 1;
    static $INACTIVE = 0;
    static $DELETE = 3;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';

    public $timestamps = true;

    protected $fillable = array('id', 'title', 'info', 'description', 'cover','views');

    public function fill(array $attributes) {
        parent::fill($attributes);
    }

    public function cover(){
        if (isset($this->cover)){
            if ($this->cover) {
                if (strripos($this->cover,'/') || strripos($this->cover,'\\')){
                    return $this->cover;
                }
                return '/images/catalogue/'.$this->cover;
            }
        }
        return '/images/none.jpg';
    }

    public function count(){
        return $this->query()->get()->count();
    }

    public function getCoverAttribute($cover){
        if (strripos($cover,':/')){
            return $cover;
        }
        if (is_file($cover)){
            return $cover;
        }
        $r = './files/';
//        $r='';
        if (is_file($r . $cover)) {
            return $r . $cover;
        }

        return '/images/none.jpg';
    }

    public function getComments(){
        return (isset($this->comments)) ? $this->comments : $this->comments();
    }
    public function comments(){
        return $this->hasMany('App\Http\Modules\Comments\Models\Comment','project_id','id')->where('status',$this::$ACTIVE)->orderBy('created_at','desc');
    }

    public function getFiles(){
        return (isset($this->files)) ? $this->files : $this->files();
    }
    public function files(){
        return $this->hasMany('App\Http\Modules\Files\Models\File','project_id','id')->where('status',$this::$ACTIVE);;
    }

}