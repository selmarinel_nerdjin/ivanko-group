<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::post('/post','Controller@go');
Route::get('/addview/{id}', 'ProjectsController@addView');
Route::get('/get_comments/{id}','CommentsController@showComments');
Route::get('/addcomment/{id}','ProjectsController@addComments');
Route::get('/v_auth', 'Controller@redirectToProvider');
Route::get('/vk_auth', 'Controller@handleProviderCallback');
Route::get('/vk_logout', 'Controller@handleProviderCallback');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'ProjectsController@index');
    Route::get('/all','ProjectsController@all');
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/adminpanel', 'HomeController@index');
    Route::get('/home', 'HomeController@index');
    Route::get('/projects', 'HomeController@index');
    Route::get('/orders', 'HomeController@orders');
    Route::get('/comments', 'CommentsController@index');
    Route::get('/comments/{id}/delete', 'CommentsController@delete');
    Route::get('/comments/{id}/active', 'CommentsController@active');
    Route::any('/register',function(){
//        return dd('Рагистрация закрыта');
        return view('welcome');
    });
    Route::get('/proj/{id}/active', 'HomeController@active');
    Route::get('/proj/{id}/delete', 'HomeController@delete');
    Route::get('/proj/add', 'HomeController@add');
    Route::post('/proj/add', 'HomeController@add');
    Route::post('/proj/{id}/add', 'HomeController@addFile');
    Route::get('/proj/{id}/edit', 'HomeController@edit');
    Route::post('/proj/{id}/edit', 'HomeController@edit');
    Route::get('/order/{id}/accept', 'HomeController@orderAccept');
    Route::get('/order/{id}/decline', 'HomeController@orderDecline');
    Route::any('/file/{id}/edit','HomeController@fileEdit');
    Route::get('/file/{id}/delete','HomeController@fileDelete');
    Route::any('/files-list','HomeController@filesIndex');
});
