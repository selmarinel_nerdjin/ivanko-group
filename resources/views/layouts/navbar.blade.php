<!-- Navigation -->
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
			<span class="label">
				@if(isset($_COOKIE['name']))
                    Здраствуйте, {{$_COOKIE['name']}}
                    <a href="/vk_logout"><i class="fa fa-sign-out"></i></a>

                @else
                    <a href="/v_auth"><i class="fa fa-sign-in"> vk</i></a>
                @endif
			</span>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li>
                    <a class="page-scroll" href="#projects">Проекты</a>
                </li>
                <li>
                    <a class="page-scroll" href="#working">Услуги компании</a>
                </li>
                <li>
                    <a class="page-scroll" href="#about">О нас</a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact">Свяжитесь с нами</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>