<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Иван,Сергей,Слипцов,Полищук,IvankoGroup,
        ivankogroup,ivanko,group,Ivanko,Group,Ivanko-Group,ivanko-group,
        обработка дерева,крафт,мебель,Selmarinel,Nerdjin">
    <meta name="description" content="Творческая мастерская ,по изготовлению , проектированию ,
            специализированных изделий под заказ , оформление деревом ,
            разработка подсветки и освящения , а так же многое другое">

    <meta name="author" content="Selmarinel"/>
    <meta name="copyright" content="©2015 Selmarinel"/>

    <title>IvankoGroup</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/css/grayscale.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/set2.css">

    <!-- Custom Fonts -->
    <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="/css/freelancer.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/favicons/manifest.json">

    <!-- jQuery -->
    <script src="/js/lib/jquery/jquery-1.11.1.min.js"></script>
    <script src="/js/lib/jquery/jquery-2.1.3.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/js/lib/bootstrap/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/js/jquery.easing.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/js/grayscale.js"></script>

    <!-- underscore -->
    <script src="/js/lib/underscore/underscore-min.js"></script>

    <!-- Pinterest GRID -->
    {{--<script src="/js/pinterest_grid.js"></script>--}}

    <script src="/js/lib/masonry.js"></script>
    <script src="/js/lib/imagesloader.js"></script>
    <script src="/js/lib/bootstrap/bootstrap.min.js"></script>
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" style="font-family:AleksandraC; ">
    @include('layouts.preloader')
    @if(\Illuminate\Support\Facades\Auth::check())
        <span class="btn btn-xs btn-info" style="position: absolute; z-index: 1278;">
            {{ Auth::user()->name }}
            <a href="/logout"><i class="fa fa-sign-out fa-btn"></i></a>
            <a href="/adminpanel"><i class="fa fa-tasks fa-btn"></i></a>
        </span>
    @endif

    @yield('content')

        <!-- Footer -->
    <footer>
    <div class="container text-center">
        <p class="raz">Copyright &copy; <a href="http://admin.ivankogroup.com">Selmarinel</a> & <strong class="raz">[VERGO]</strong> 2016</p>
    </div>
</footer>
<!-- Loading catalogue -->
<script>
    function setMasonry(){
        var $grid = $('.grid');
        $grid.imagesLoaded().progress( function() {
            $grid.masonry({
                animate: true,
                columnWidth: '.grid-item',
                percentPosition: true,
                isFitWidth: true,
                stamp: '.stamp',
                itemSelector : '.grid-item'
            });
        });
    }
    $(document).ready(function(){
        var $grid_item = $('.grid-item').width();
        $('.grid-item').width($grid_item-10);
        setMasonry();
        $('.text-block').hide();
    });
    $(window).resize(function(){
        setMasonry();
    });
    $('.about-button').on('click',function(){
        var ni = $('.ni').val();
        var ti = $('.ti').val();
        var mi = $('.mi').val();
        $.ajax({
            type: "GET",
            url: '/email?ni=' + ni + '&ti=' + ti + '$mi=' + mi,
            contentType: false,

            success: function (res) {
                console.log(res);
                alert('!');
            }
        });
    });
    $('.block').on('mousemove',function(){
        $(this).find('.text-block').fadeIn(300);
        $(this).find('.name').addClass('padi');
    });
    $('.block').on('mouseleave',function(){
        $(this).find('.text-block').fadeOut(300);
        $(this).find('.name').removeClass('padi');
    });
    $('.project_item').on('click',function(){
        $id = $(this).val();

        $.ajax({
            type: "GET",
            url: "/addview/"+$id,
            success: function(res){
            },
            error:function(res){
                console.log('error')
            }
        });
    });
    var comment = _.template('<div class="comments col-xs-12 panel text-left">'+
            '<div class="col-xs-2">'+
            '<img src="<%= image %>" class="img-responsive img-circle">'+
            '<span class="text-center"><%= user %></span>'+
            '</div>'+
            '<div class="col-xs-10 comment-text"><%= commenttext %></div>'+
            '</div>');

    $('.comment-button').on('click',function(){
        var $comment_id = $(this).attr('id');
        var $comment_user = $(this).parent().find('#nam'+$comment_id).val();
        var $comment_img = $(this).parent().find('#img'+$comment_id).val();
        var $comment_text = $(this).parent().find('#tex'+$comment_id).val();
        var $parent = $(this).parent().parent().parent().find('.modal-footer');
        if($comment_id && $comment_user && $comment_text){
            $.ajax({
                type: "GET",
                url: "/addcomment/"+$id+"?user="+$comment_user+"&text="+$comment_text+"&avatar="+$comment_img,
                success: function(res){
                    $parent.prepend(comment({user:$comment_user,commenttext:$comment_text,image:$comment_img}));
                    return console.log('added');
                },
                error:function(res){
                    console.log('error')
                }
            });
//            $(this).parent().find('#nam'+$comment_id).val('');
            $(this).parent().find('#tex'+$comment_id).val('');
        }
        return console.log('invalid');
    })
</script>
</body>

</html>