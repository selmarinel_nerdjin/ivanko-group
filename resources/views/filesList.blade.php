@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-10">
                <h2>Файлы <span class="badge">
                        @if($collection->first())
                            {{$collection->first()->count()}}
                        @endif
                    </span></h2>
            </div>
            <div class="col-lg-2">
                <a href="/proj/add" class="text-right" ><span class="btn btn-info">Создать проэкт</span></a>
            </div>
        </div>
        <div class="text-center"> <?php echo $collection->render(); ?></div>
        <div class="clearfix"></div>
            @foreach($collection as $model)
                <div class="@if($model->status) alert-info @else alert-danger @endif alert col-lg-12">
                    <div class="content">
                        <div class="col-lg-6">
                            <strong>{{$model->name}}</strong><br/>
                            <small>{{$model->text}}</small><br/>
                            <small class="label label-info">{{$model->created_at}}</small><br/>
                        </div>
                        <div class="col-lg-2">
                            <img class="img-responsive img-thumbnail" src="{{$model->cover}}">
                        </div>
                        <div class="col-lg-2">
                            <div class="col-xs-12">
                                <h3>ПРОЕКТ</h3>
                                <img src="{{$model->project['cover']}}" class="img-responsive">
                            </div>
                            <div class="col-xs-12">
                                {{$model->project['title']}}
                            </div>
                        </div>
                        <div class="col=lg-2 text-right">
                            <a href="/file/{{$model->id}}/edit">
                                <span class="btn btn-info">
                                    <i class="fa fa-edit"></i>
                                </span>
                            </a>
                            <a href="/file/{{$model->id}}/delete">
                               <span class="btn @if($model->status) btn-warning @else btn-success @endif">
                                    <i class="fa @if($model->status) fa-eye-slash @else fa-eye @endif "></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
</div>
@endsection
