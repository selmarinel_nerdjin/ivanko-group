@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-10">
                <h2>Проекты <span class="badge">
                        @if($collection->first())
                            {{$collection->first()->count()}}
                        @endif
                    </span></h2>
            </div>
            <div class="col-lg-2">
                <a href="/proj/add" class="text-right" ><span class="btn btn-info">Создать проэкт</span></a>
            </div>
        </div>
        <?php echo $collection->render(); ?>
        <div class="clearfix"></div>
            @foreach($collection as $model)
                <div class="@if($model->status) alert-info @else alert-danger @endif alert col-lg-12">
                    <div class="content">
                        <div class="col-lg-2">
                            <strong>{{$model->title}}</strong><br/>
                            <span class="label label-info"><i class="fa fa-eye"></i> {{$model->views}}</span>
                            <span class="label label-primary"><i class="fa fa-comments"></i> {{$model->comments->count()}}</span>
                            <span class="label label-success"><i class="fa fa-image"></i> {{$model->files->count()}}</span>
                        </div>
                        <div class="col-lg-6">
                            {{$model->description}}
                        </div>
                        <div class="col-lg-2">
                            <img class="img-responsive img-thumbnail" src="{{$model->cover}}">
                        </div>
                        <div class="col=lg-2 text-right">
                            <a href="/proj/{{$model->id}}/delete">
                                <span class="btn btn-danger">
                                    <i class="fa fa-trash"></i>
                                </span>
                            </a>
                            <a href="/proj/{{$model->id}}/active">
                                <span class="btn @if($model->status) btn-warning @else btn-success @endif">
                                    <i class="fa @if($model->status) fa-eye-slash @else fa-eye @endif "></i>
                                </span>
                            </a>
                            <a href="/proj/{{$model->id}}/edit">
                                <span class="btn btn-info">
                                    <i class="fa fa-edit"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
    </div>
    </div>
</div>
@endsection
