@extends('layouts.main')

@section('content')

@include('layouts.navbar')

        <!-- Intro Header -->
<header class="intro">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="col-xs-4 brand-image"><img src="/images/Em.png" alt="emblem" id="Em" class="img-responsive"></div>
                    <h1 class="brand-heading">IvankoGroup</h1>
                    <p class="intro-text" style="font-family: Avdiva;">МЫ – КОМПАНИЯ, КОТОРАЯ ПРОИЗВОДИТ ХОРОШИЕ И ИНТЕРЕСНЫЕ ИЗДЕЛИЯ.</p>
                </div>
                <div class="col-xs-12 linker">
                    <a href="#projects" class="btn btn-circle page-scroll">
                        <i class="fa fa-angle-down animated"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Projects section -->
<section id="projects" class="container content-section text-center">
    <div class="row">
        <div class="col-lg-12"><h2 class="custom_title">Проекты</h2></div>
        @if(empty($collection))
            <div class="col-lg-12 text-center"><h1>Извините, но возникли неполадки в базе, или проэкты еще не загружены</h1></div>
        @endif
        <div class="container marketing">
            <section id="blog-landing" class="grid">
                @foreach($collection as $model)
                    <article class="white-panel text-center grid-item col-md-4 col-sm-6 col-xs-12">
                        <figure class="block">
                            <img src="{{$model->cover()}}" class="img-responsive">
                            <span class="name">{{$model->title}}</span>
                            <figcaption class="text-block">
                                <div class="text">
                                    {{mb_substr($model->description, 0, 65, 'UTF-8') . '...'}}
                                </div>
                                <p class="link">
                                    <button type="button" class="btn btn-more project_item" data-toggle="modal" value="{{$model->id}}" data-target="#project{{$model->id}}">
                                        Подробнее
                                    </button>
                                </p>
                                <span class="views"><i class="fa fa-eye"></i> {{$model->views}}</span>
                                <span class="proj_comments"><i class="fa fa-comments"></i> {{$model->comments->count()}}</span>
                            </figcaption>
                        </figure>
                    </article>
                @endforeach
            </section>
        </div>
    </div>
</section>

<section>
    @foreach($collection as $model)
        <div class="modal fade" id="project{{$model->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{$model->title}}</h4>
                    </div>
                    <div class="img-res">
                        <img src="{{$model->cover}}" class="">
                    </div>
                    <div class="modal-body">
                        <?php echo $model->description; ?>
                    </div>
                    <div class="modal-footer">
                        @foreach($model->comments as $comment)
                            <div class="comments col-xs-12 panel text-left">
                                <div class="col-xs-2">
                                    <img src="{{$comment->getCover()}}" class="img-responsive img-circle">
                                    <span class="text-center">{{$comment->user}}</span>
                                </div>
                                <div class="col-xs-10 comment-text">{{$comment->text}}</div>
                            </div>
                        @endforeach
                        <form class="form-group text-left">
                            <label class="{{(isset($_COOKIE['name']))?'hidden':''}}">Ваше Имя</label>
                            <input type="text" class="form-control {{(isset($_COOKIE['name']))?'hidden':''}}" required id="nam{{$model->id}}"
                                   value="{{(isset($_COOKIE['name']))?$_COOKIE['name']:null}}">
                            <input type="text" class="hidden" value="{{(isset($_COOKIE['img']))?$_COOKIE['img']:'/images/ava.png'}}" id="img{{$model->id}}" >
                            <label class="">Ваш комментарий</label>
                            <textarea class="form-control" required id="tex{{$model->id}}"></textarea>
                            <input type="button" value="Оставить комментарий" class="btn btn-more comment-button" id="{{$model->id}}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</section>
<br/>
<div class="col-lg-12 text-center">
    <a href="/all"><span class="btn btn-link btn-more">Показать все</span> </a>
</div>

<!-- HR -->
<section class="content-section text-center">
    <div class="content-section text-center" id="hr">
        <div class="container">
            <div class="row">
                <img style="width: 220px;" src="./images/Em.png">
                <h2>Ivanko Group</h2>
                <p>Это обитель добрых гениев, способных воплотить в реальность изделия из любых материалов,
                    дабы принести частичку волшебства и уюта в Ваш дом.</p>
            </div>
        </div>
    </div>
</section>

<!-- WORKING Section -->
<section id="working" class="content-section text-center container">
    <div class="content row">
        <div class="col-lg-12"><h2 class="custom_title">Услуги компании</h2></div>
        <div class="col-lg-12">
            <ul class="timeline">
                <li>
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="/images/us/1.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="subheading"><i class="fa fa-paint-brush"></i> ГОТОВАЯ ПРОДУКЦИЯ НА ПРОДАЖУ</h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted">
                                Вы можете приобрести уже готовую продукцию, произведённую в нашей мастерской
                            </p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="/images/us/2.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="subheading"><i class="fa fa-briefcase"></i> РАЗРАБОТКА И ОФОРМЛЕНИЕ</h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted">
                                Разработка концепции и изготовление элементов фасадных витрин и интерьеров ( мебель, арт-объекты  декора, силовые конструкции).
                            </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="/images/us/3.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="subheading"><i class="fa fa-wrench"></i> ПРОИЗВОДСТВО</h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted">
                                При создании нашей продукции мы используем древесину различных пород, а также комбинируем ее с другими видами материалов. Качество европейское -  цены наши.
                            </p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="/images/us/4.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="subheading"><i class="fa fa-file-text"></i> ГАРАНТИЯ</h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted">
                                Мы гарантируем качество любой продукции, созданной в нашей мастерской.
                            </p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>

<!-- Download Section -->
<section id="download" class="content-section text-center">
    <div class="download-section">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2">
                <h3>Закажите уникальные изделия прямо сейчас</h3>
                <p>Оставьте заявку и мы свяжемся с вами в ближайшее время</p>
                <a href="#contact" class="btn btn-success btn-default page-scroll">Заказать!</a>
            </div>
        </div>
    </div>
</section>

<!-- About Section -->
<section id="about" class="container content-section text-center">
    <div class="row">
        <div class="col-lg-12"><h2 class="custom_title">ПОЧЕМУ ИМЕННО МЫ</h2></div>
        <!--<div class="col-lg-12"><h3>Мы никогда не сидим без дела!</h3></div>-->
        <div class="col-lg-12">
            <p>
                Наша команда состоит из идейных специалистов, любящих свое дело. Каждый из нас прошёл долгий путь в разной производственной деятельности. Продукция, которую создает мастерская, имеет качество и гарантии в своем сегменте. В нашей компании работаю умельцы с золотыми руками и ясной головой в области создания, дизайна, проектирования, и внедрения деревянных изделий. Взаимопонимание с каждым клиентом обеспечивает создание уникального продукта с гарантией от нашей компании.
                <br/>
                Мастера компании Ivanko Group имеют опыт работы в создании мебели которая создаст уют, и будет радовать Вас каждый день.
            </p>
        </div>
    </div>
    <div class="clearfix"><br></div>
    <div class="row">
        <div class="col-lg-12">
            <ul class="timeline">
                <li>
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="/images/team/6.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="subheading">ОПЫТНЫЕ СПЕЦИАЛИСТЫ</h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted">
                                В нашей мастерской, работают специалисты любящие свое дело. Они вкладывают душу в каждое изделие и всегда рады порадовать заказчика.
                            </p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="/images/team/7.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="subheading">БОЛЬШЕ ВОЗМОЖНОСТЕЙ</h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted">
                                Вы можете получить уже готовое изделие просто приехав к нам в шоу рум.
                            </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-image">
                        <img class="img-circle img-responsive" src="/images/team/8.jpg" alt="">
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="subheading">ИНДИВИДУАЛЬНЫЙ ПОДХОД</h4>
                        </div>
                        <div class="timeline-body">
                            <p class="text-muted">
                                Мы всегда рады рассказать заказчику о техническом подходе к вопросу изготовления продукта, чтобы он мог понять и поучаствовать в изготовлении своего заказа. Также вы можете прийти со своей идеей, которую мы с удовольствием воплотим в жизнь.
                            </p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>

<!-- Map Section -->
<section id ="mapSection" class="embed-container  maps">
    <div id="map" style="pointer-events: none;"></div>
</section>
<!-- Contact Section -->
<section id="contact" class="content-section text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="col-lg-12">
                    <h3 class="bebas" style="color:#eee">Свяжитесь с нами</h3>
                </div>
                <div class="col-xs-3"><a href="https://www.facebook.com/ivankogroup" class="btn btn-dark btn-lg" target="_blank"><i class="fa fa-facebook fa-2x"></i></a></div>
                <div class="col-xs-3"><a href="https://www.instagram.com/ivankogroup/" class="btn btn-dark btn-lg" target="_blank"><i class="fa fa-instagram fa-2x"></i></a></div>
                <div class="col-xs-3"><a href="https://plus.google.com/115226412993562348306" class="btn btn-dark btn-lg" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a></div>
                <div class="col-xs-3"><a href="https://vk.com/ivankogroup" class="btn btn-dark btn-lg"><i class="fa fa-vk fa-2x" target="_blank"></i></a></div>
                <div class="col-xs-6" style="color:#eee"><h5>(095) 594 41 63</h5></div>
                <div class="col-xs-6" style="color:#eee"><h5>(067) 900 35 75</h5></div>
            </div>
            <form method="POST" action="/post">
                <div class="col-lg-4">
                    <div class="input-div"><input name="name" class="about-text-input ni" placeholder="Ваше имя" type="text" required value="{{isset($_COOKIE['name']) ? $_COOKIE['name'] : '' }}"></div>
                    <div class="input-div"><input name="phone" class="about-text-input ti" placeholder="Ваш телефон +3 (012) 345-67-89" type="tel" required></div>
                    <div class="input-div"><input name="mail" class="about-text-input mi" placeholder="Ваш e-mail" type="text"></div>
                </div>
                <div class="col-lg-4">
                    <div class="input-div">
                        <textarea class="about-textarea" name="text" required placeholder="Введите подробности заказа"></textarea>
                        <input type="submit" class="about-button" value="Отправить">
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALQk6TeigssrTVIg-pxowMLhaXxGiRfoY&callback=initMap"
        async defer></script>
<script>
    // Disable scroll zooming and bind back the click event
    var onMapMouseleaveHandler = function (event) {
        var that = $(this);

        that.on('click', onMapClickHandler);
        that.off('mouseleave', onMapMouseleaveHandler);
        that.find('#map').css("pointer-events", "none");
    }

    var onMapClickHandler = function (event) {
        var that = $(this);

        // Disable the click handler until the user leaves the map area
        that.off('click', onMapClickHandler);

        // Enable scrolling zoom
        that.find('#map').css("pointer-events", "auto");

        // Handle the mouse leave event
        that.on('mouseleave', onMapMouseleaveHandler);
    }

    // Enable map zooming with mouse scroll when the user clicks the map
    $('.maps.embed-container').on('click', onMapClickHandler);
</script>
@stop