
@extends('layouts.main')
<link rel="stylesheet" href="/css/bootstrap-image-gallery.css">
@section('content')
    <nav class="navbar navbar-inverse" style="height: 75px">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="/images/Em.png" class="img-circle" style="width: 50px;">Ivanko Group
                </a>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row" id="project-row">
            @foreach($collection as $model)
                <div class="project-block col-xs-12">
                    <div class="col-xs-12">
                        <div class="col-xs-12">
                            <div class="imgres" style="position: relative;">
                                <img src="{{$model->cover}}" class="img-responsive" style="margin: 0 auto">
                                <div class="name">
                                    <h2 class="modal-title">{{$model->title}}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="text-left modal-body">
                            {{$model->description}}
                        </div>
                        <div class="hidden">{{$model->id}}</div>
                    </div>
                    @if($model->files->first())
                        <h2 class="custom_title">Галерея</h2>
                    <div class="col-xs-12">
                        <div class="grid">
                        @foreach($model->files as $file)
                            <div class="grid-item col-sm-4 col-xs-6" style="padding: 0px">
                                <!-- Large modal -->
                                <a type="button" class="" data-toggle="modal" data-target="#file{{$file->id}}">
                                <img src="{{$file->cover}}" class="img-responsive img-thumbnail">
                                <div class="name">
                                    <h5 class="modal-title">
                                        {{$file->name}}
                                    </h5>
                                </div>
                                </a>
                                <div class="modal fade bs-example-modal-lg" id="file{{$file->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header img-res">
                                                <h4 class="modal-title name">
                                                    {{$file->name}}
                                                </h4>
                                                <img src="{{$file->cover}}" class="img-responsive" style="margin: 0 auto;">
                                            </div>
                                            <div class="modal-body text-left">
                                                {{$file->text}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    </div>
                    @endif
                    <div class="col-xs-12">
                        <div class="modal-footer">
                        @foreach($model->comments as $comment)
                            <div class="comments col-xs-12 panel text-left">
                                <div class="col-xs-2">
                                    <img src="/images/ava.png" class="img-responsive img-circle">
                                    <span class="text-center">{{$comment->user}}</span>
                                </div>
                                <div class="col-xs-10 comment-text">{{$comment->text}}</div>
                            </div>
                        @endforeach
                        <form class="form-group text-left">
                            <label class="">Ваше Имя</label>
                            <input type="text" class="form-control" required id="nam{{$model->id}}">
                            <label class="">Ваш комментарий</label>
                            <textarea class="form-control" required id="tex{{$model->id}}"></textarea>
                            <input type="button" value="Оставить комментарий" class="btn btn-more comments-button" id="{{$model->id}}">
                        </form>
                    </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="text-center"> <?php echo $collection->render(); ?></div>
    </div>
    <script>
        $('.comments-button').on('click',function(){
            var $comment_id = $(this).attr('id');
            var $comment_user = $(this).parent().find('#nam'+$comment_id).val();
            var $comment_text = $(this).parent().find('#tex'+$comment_id).val();
            var $parent = $(this).parent().parent().parent().find('.modal-footer');
            var $id = $(this).parent().parent().parent().find('.hidden').text();
            if($comment_id && $comment_user && $comment_text){
                $.ajax({
                    type: "GET",
                    url: "/addcomment/"+$id+"?user="+$comment_user+"&text="+$comment_text,
                    success: function(res){
                        $parent.prepend(comment({user:$comment_user,commenttext:$comment_text}));
                        return console.log('added');
                    },
                    error:function(res){
                        console.log('error')
                    }
                });
                $(this).parent().find('#nam'+$comment_id).val('');
                $(this).parent().find('#tex'+$comment_id).val('');
            }
            return console.log('invalid');
        })
    </script>
@stop