@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <form method="Post" enctype = "multipart/form-data">
                <input type="text" class="hidden" name="id" value="{{$model->id}}">
                <div class="form-group">
                    <label for="title">Название проэкта</label>
                    <input type="text" class="form-control" name="title" id="title" placeholder="Название" value="{{$model->title}}" required>
                </div>
                {{--<div class="form-group">--}}
                    {{--<label for="cover">Ссылка на изображение проэкта</label>--}}
                    {{--<input type="text" class="form-control" name="cover" id="cover" placeholder="Ссылка">--}}
                {{--</div>--}}
                <div class="form-group">
                    <label for="cover">Полное описание</label>
                    <textarea class="form-control" rows="3" name="description" required>{{$model->description}}</textarea>
                </div>
                <div class="form-group">
                    <input type="file" name="file_cover" value="{{$model->cover}}">
                </div>
                <input type="submit" title="Создать" class="btn btn-success">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            </form>

            @if($model->id)
            @foreach($model->files as $file)
                <div class="col-xs-4">
                    <div class="col-xs-12">
                        <img src="{{$file->cover}}" class="img-thumbnail img-responsive">
                        <div class="carousel-caption">
                            <h2>{{$file->name}}</h2>
                        </div>
                    </div>
                    <pre>{{$file->text}}</pre>
                    <div class="col-xs-12">
                        <a href="/file/{{$file->id}}/edit"><span class="btn btn-danger">Редактировать</span></a>
                    </div>
                </div>
            @endforeach
            <div class="col-xs-4">
                <form method="POST" action="/proj/{{$model->id}}/add" enctype = "multipart/form-data" class="form-horizontal">
                    <input type="text" name="name" class="form-control" placeholder="Название" required>
                    <textarea name="text" class="form-control" placeholder="Описание"></textarea>
                    <input type="file" class="" value="Фото" accept="image/*" name="new_photo" required>
                    <input type="submit" class="btn btn-success" value="Добавить фото" >
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                </form>
            </div>
            @endif
        </div>
    </div>
@endsection