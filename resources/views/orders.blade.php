@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Заказы <span class="badge">
                        @if($collection->first())
                            {{$collection->first()->count()}}
                        @endif
                    </span></h2>
            </div>
            @foreach($collection as $model)
                <div class="col-lg-12 alert {{$model->getStateStyle()}}">
                    <div class="col-lg-2">{{$model->name}}</div>
                    <div class="col-lg-2">{{$model->phone}}</div>
                    <div class="col-lg-2">{{$model->mail}}</div>
                    <div class="col-lg-3">{{$model->text}}</div>
                    <div class="col-lg-2">{{$model->getStateName()}}</div>
                    <div class="col-lg-1 text-right">
                        @if($model->status != \App\Http\Modules\Order\Models\Order::$Complete && $model->status != App\Http\Modules\Order\Models\Order::$EMailFail)
                            <a href="/order/{{$model->id}}/accept">
                                <span class="btn btn-primary">
                                    @if($model->status == \App\Http\Modules\Order\Models\Order::$InVoice)
                                        Выполнить
                                    @elseif($model->status == \App\Http\Modules\Order\Models\Order::$InProcess)
                                        Закончить
                                    @endif
                                </span>
                            </a>

                            <a href="/order/{{$model->id}}/decline">
                                <span class="btn btn-danger">Отказаться</span>
                            </a>
                        @endif
                    </div>
                </div>
            @endforeach
            <?php echo $collection->render(); ?>
        </div>
    </div>

@endsection