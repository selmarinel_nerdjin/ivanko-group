@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <form method="Post" enctype = "multipart/form-data">
                <input type="text" class="hidden" name="id" value="{{$model->id}}">
                <div class="form-group">
                    <label for="name">Имя фотографии</label>
                    <input type="text" class="form-control" name="name" placeholder="Название" value="{{$model->name}}" required>
                </div>
                <div class="form-group">
                    <label for="text">Описание фото</label>
                    <textarea class="form-control" rows="3" name="text">{{$model->text}}</textarea>
                </div>
                <div class="form-group">
                    <label for="last_cover">Существующая фотография</label><br/>
                    <img src="{{$model->cover}}" width="300" name="last_cover">
                </div>
                <div class="form-group">
                    <label for="file_cover">Или выбрать новую</label><br/>
                    <input type="file" name="file_cover" value="{{$model->cover}}">
                </div>
                <input type="submit" title="Создать" class="btn btn-success">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            </form>
        </div>
    </div>
@endsection