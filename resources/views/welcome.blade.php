<!DOCTYPE html>
<html>
<head>
    <title>Ivanko Group | CLOSE REGISTRATION</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }
        @font-face {
            font-family: NotCourier; /* Имя шрифта */
            src: url(fonts/notcourier.ttf); /* Путь к файлу со шрифтом */
        }
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: NotCourier;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }
        .raz {
            background: linear-gradient(#600060, #006000);  /* подробнее про градиенты */
            -webkit-background-clip: text;  /* подробнее про background-clip */
            color: transparent;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title raz">
            [ Регистрация закрыта ]
        </div>
            <a href="/">Назад</a>
    </div>
</div>
</body>
</html>

